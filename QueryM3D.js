;(function (global) {

	/**
	 * Método para recorrer una matriz 3D en los rangos dados y sumar los
	 * elementos encontrados.
	 * @param  int x1 Coordenada del rango a buscar
	 * @param  int y1 Coordenada del rango a buscar
	 * @param  int z1 Coordenada del rango a buscar
	 * @param  int x2 Coordenada del rango a buscar
	 * @param  int y2 Coordenada del rango a buscar
	 * @param  int z2 Coordenada del rango a buscar
	 * @return int    Valor de la suma de los elementos en el rango
	 */
	var query = function (x1, y1, z1, x2, y2, z2) {
		x1--;
		y1--;
		z1--;
		x2--;
		y2--;
		z2--;

		var sum = 0;
		var x = x2 - x1 + 1;
		var y = y2 - y1 + 1;
		var z = z2 - z1 + 1;

		for(var i = 0; i < x; i++){
		   for(var j = 0; j < y; j++){
			   for(var k = 0; k < z; k++){
					sum += this.M[x1+i][y1+j][z1+k];
			   }
		   }
		}

		return sum;
	}

	global.prototype.query = query;
})(Matrix3D);
// Se llama con Matrix3D para que query quede como
// una propiedad de Matrix3D y no en el scope global.
