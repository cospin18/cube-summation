QUnit.module( "CubeSummation" );

QUnit.test( "Prueba de entrada y salida", function( assert ) {

	var test = "2\n4 5\nUPDATE 2 2 2 4\nQUERY 1 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3\n2 4\nUPDATE 2 2 2 1\nQUERY 1 1 1 1 1 1\nQUERY 1 1 1 2 2 2\nQUERY 2 2 2 2 2 2";
	var CS = CubeSummation(test);
	assert.deepEqual( CS, [4, 4, 27, 0, 1, 1], "Resultado para el input de prueba uno" );


	var test2 = "7\n4 5\nUPDATE 2 2 2 4\nQUERY 1 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3\n2 4\nUPDATE 2 2 2 1\nQUERY 1 1 1 1 1 1\nQUERY 1 1 1 2 2 2\nQUERY 2 2 2 2 2 2\n4 5\nUPDATE 2 2 2 4\nQUERY 1 1 1 3 3 3\nUPDATE 1 2 3 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3\n3 4\nUPDATE 2 2 2 1\nQUERY 1 1 1 1 1 1\nQUERY 1 1 1 2 2 2\nQUERY 3 3 3 3 3 3\n20 5\nUPDATE 2 2 2 1\nQUERY 1 1 1 2 2 2\nQUERY 20 20 20 20 20 20\nUPDATE 20 20 20 1\nQUERY 20 20 20 20 20 20\n32 15\nQUERY 27 8 20 31 21 31\nUPDATE 20 28 21 321\nQUERY 1 1 1 32 32 32\nQUERY 1 1 1 32 32 32\nUPDATE 8 18 2 46465\nUPDATE 31 21 31 8978\nUPDATE 4 11 21 23132\nQUERY 1 1 1 32 32 32\nQUERY 1 1 1 32 32 32\nQUERY 1 1 1 32 32 32\nQUERY 25 13 8 27 21 25\nQUERY 1 1 1 32 32 32\nUPDATE 7 11 4 5645\nQUERY 1 1 1 32 32 32\nQUERY 1 1 1 32 32 32\n10 5\nUPDATE 9 9 9 4\nQUERY 1 1 1 3 3 3\nUPDATE 5 5 5 45\nUPDATE 2 3 5 80\nQUERY 2 2 2 7 7 7\nQUERY 1 1 1 10 10 10";
	var CS2 = CubeSummation(test2);
	assert.deepEqual( CS2, [4, 4, 27, 0, 1, 1, 4, 4, 27, 0, 1, 0, 1, 0, 1, 0, 321, 321, 78896, 78896, 78896, 0, 78896, 84541, 84541, 0, 125], "Resultado para el input de prueba dos" );


});