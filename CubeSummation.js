;(function (global) {
	/**
	 * Obtiene la entrada en el formato especifico y la procesa
	 * @param str input Cadena a procesar
	 * @return [int] Resultados
	 */
	var CubeSummation = function (input) {
		var result = [];

		var t2 = 0;
		var lines = input.split("\n");
		var numTestCases = lines.splice( 0, 1 );

		for(var i = 0;i < numTestCases;i++){

		    var testInfo = lines[0].split(" ");
		    var N = parseInt(testInfo[0]);
		    var M = parseInt(testInfo[1]);

			var matrix3D = Matrix3D(N);
			lines.splice(0, 1);

		    for (var j = 0; j < M; j++) {

		    	var actionData = lines[j].split(" ");
		    	var action = actionData.splice(0, 1).toString();
		    	actionData = actionData.map(function (n) {
		    		return parseFloat(n);
		    	});

		    	switch(action) {
				    case "UPDATE":
				    	var [x, y, z, w] = actionData;
				        matrix3D.update(x, y, z, w);
				        break;
				    case "QUERY":
				    	var [x1, y1, z1, x2, y2, z2] = actionData;
				    	var sum = matrix3D.query(x1, y1, z1, x2, y2, z2);
				    	result.push(sum);
				        console.log(sum);
				        break;
				    default:
				        throw "Unexpected action: "+action;
				};
		    }
		    lines.splice(0, M);

		}
		return result;
	}

	global.CubeSummation = CubeSummation;
})(window); // 'global' en NodeJS
