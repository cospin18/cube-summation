<?php

public function post_confirm(){
	$serviceID = Input::get('service_id');
	$service = Service::find($serviceID);

	if($service){
		if($service->status_id == 6){
			return Response::json(array('error' => 2));

		}else if($service->status_id == 1 && !$service->driver_id) {

			$driverID = Input::get('driver_id');
			$driverTemp = Driver::find($driverID);

			$service = Service::update($serviceID, array(
				'driver_id' => $driverID,
				'car_id' => $driverTemp->car_id,
				'status_id' => '2',
				//'pwd' => md5(Input::get('pwd')) //Up Carro
			));

			Driver::update($driverID, array(
				"available" => '0'
			));

			if($service->user->uuid != ''){
				$push = Push::make();
				$pushMessage = 'Tu servicio ha sido confirmado!';

				if($service->user->type == '1'){//iPhone
					//$push->ios($service->user->uuid, $pushMessage);
					$push->ios($service->user->uuid, $pushMessage, 1, 'honk.wav', 'Open', array('serviceId' => $service->id));
				}else{
					//$push->android($service->user->uuid, $pushMessage);
					$push->android2($service->user->uuid, $pushMessage, 1, 'default', 'Open', array('serviceId' => $service->id));
				}
			}

			return Response::json(array('error' => 0));
		}

  		return Response::json(array('error' => 1));
	}

  	return Response::json(array('error' => 3));
}