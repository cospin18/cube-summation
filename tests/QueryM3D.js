QUnit.module( "QueryM3D" );

QUnit.test( "Query correcto", function( assert ) {

	var mat = Matrix3D(4);
	// Se modifica la matriz para no depende de UPDATE
	mat.M = [[[1,4,0,0],[0,0,0,0],[0,0,0,0],[9,0,2,0]],[[0,40,0,5],[0,0,0,0],[0,0,0,0],[0,0,0,0]],[[5,1,3,0],[0,7,15,88],[0,65,0,0],[0,0,0,7]],[[0,0,9,0],[0,0,10,0],[0,0,0,0],[4,1,1,3]]];

	assert.equal( mat.query(1, 1, 1, 1, 1, 1), 1, "La suma entre  1 1 1 y 1 1 1 debe ser 1" );
	assert.equal( mat.query(1, 1, 1, 2, 2, 2), 45, "La suma entre  1 1 1 y 2 2 2 debe ser 45" );
	assert.equal( mat.query(1, 1, 1, 3, 3, 3), 141, "La suma entre  1 1 1 y 3 3 3 debe ser 141" );
	assert.equal( mat.query(1, 1, 1, 4, 4, 4), 280, "La suma entre  1 1 1 y 4 4 4 debe ser 280" );
	assert.equal( mat.query(2, 2, 2, 3, 3, 3), 87, "La suma entre  2 2 2 y 3 3 3 debe ser 87" );
	assert.equal( mat.query(2, 2, 2, 4, 4, 4), 197, "La suma entre  2 2 2 y 4 4 4 debe ser 197" );


});