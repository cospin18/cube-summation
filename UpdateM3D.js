;(function (global) {

	/**
	 * Actualiza una posición de una matriz 3D al valor deseado
	 * @param  int x Coordenada x
	 * @param  int y Coordenada y
	 * @param  int z Coordenada z
	 * @param  int w Valor a poner en la posición
	 */
	var update = function (x, y, z, w) {
		x--;
		y--;
		z--;

		this.M[x][y][z] = w;
	};
	global.prototype.update = update;

})(Matrix3D);
