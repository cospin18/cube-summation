var run = document.getElementById("run");
run.addEventListener("click", function () {
	var input = document.getElementById("input").value;
	var result = CubeSummation(input);

	var div = document.getElementById('result');
	div.innerHTML = "<h2>Resultado:</h2>";

	for (var i = 0; i < result.length; i++) {
		div.innerHTML = div.innerHTML + result[i] + '<br>';
	}

});