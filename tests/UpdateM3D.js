QUnit.module( "UpdateM3D" );

QUnit.test( "Update de la matriz", function( assert ) {

	var mat = Matrix3D(4);

	mat.update(1, 1, 1, 100)
	assert.equal( mat.M[0][0][0], 100, "'1, 1, 1, 100' debe dejar la posición 0, 0, 0 de la matriz en 100" );

	mat.update(1, 2, 3, 4)
	assert.equal( mat.M[0][1][2], 4, "'1, 2, 3, 4' debe dejar la posición 0, 1, 2 de la matriz en 4" );

	mat.update(1, 2, 3, 5)
	assert.equal( mat.M[0][1][2], 5, "'1, 2, 3, 4' debe dejar la posición 0, 1, 2 de la matriz en 5" );

	mat.update(4, 4, 4, 7)
	assert.equal( mat.M[3][3][3], 7, "'4, 4, 4, 7' debe dejar la posición 3, 3, 3 de la matriz en 7" );

	mat.update(4, 2, 4, 65)
	assert.equal( mat.M[3][1][3], 65, "'4, 2, 4, 65' debe dejar la posición 3, 1, 3 de la matriz en 65" );

	mat.update(4, 4, 4, 500)
	assert.equal( mat.M[3][3][3], 500, "'4, 4, 4, 500' debe dejar la posición 3, 3, 3 de la matriz en 500" );


});