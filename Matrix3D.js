;(function (global) {
	/**
	 * Objeto que genera y contiene la matriz cubica para su posterior uso.
	 * @param  int N Permite crear la matriz N * N * N
	 */

	var Matrix3D = function (N) {
		return new Matrix3D.init(N);
	}

	var generate = function (N) {
		var matrix = new Array(N);
		for (var i = 0; i<N; i++){
			matrix[i] = new Array(N);
			for (var j = 0; j<N; j++){
				matrix[i][j] = new Array(N).fill(0);
			};
		};
		return matrix;
	}

	Matrix3D.init = function (N) {
		var self = this;

		self.M = generate(N);
	}

	Matrix3D.init.prototype = Matrix3D.prototype;
	global.Matrix3D = Matrix3D;
})(window); // 'global' en NodeJS